# set path to application
# app_dir = File.expand_path("../..", __FILE__)
# shared_dir = "#{app_dir}/shared"
# working_directory app_dir


# # Set unicorn options
# worker_processes 2
# preload_app true
# timeout 30

# # Set up socket location
# listen "#{shared_dir}/sockets/unicorn.sock", :backlog => 64

# # Logging
# stderr_path "#{shared_dir}/log/unicorn.stderr.log"
# stdout_path "#{shared_dir}/log/unicorn.stdout.log"

# # Set master PID location
# pid "#{shared_dir}/pids/unicorn.pid"


########################################################################################

# Unicorn configuration file to be running by unicorn_init.sh with capistrano task
# read an example configuration before: http://unicorn.bogomips.org/examples/unicorn.conf.rb
# 
# working_directory, pid, paths - internal Unicorn variables must to setup
# worker_process 4              - is good enough for serve small production application
# timeout 30                    - time limit when unresponded workers to restart
# preload_app true              - the most interesting option that confuse a lot of us,
#                                 just setup is as true always, it means extra work on 
#                                 deployment scripts to make it correctly
# before_fork, after_fork       - reconnect to all dependent services: DB, Redis, Sphinx etc.
#                                 deal with old_pid only if CPU or RAM are limited enough


working_directory "/var/www/apps/ayurvedau_production/current"
pid               "/var/www/apps/ayurvedau_production/shared/tmp/pids/unicorn.pid"
stderr_path       "/var/www/apps/ayurvedau_production/shared/log/avcommerce_server_unicorn.error.log"
stdout_path       "/var/www/apps/ayurvedau_production/shared/log/avcommerce_server_unicorn.access.log"

listen            "/tmp/unicorn.avcommerce.production.sock"
worker_processes  4
timeout           30
preload_app       true


before_fork do |server, worker|
  if defined?(ActiveRecord::Base)
    ActiveRecord::Base.connection.disconnect!
  end

  old_pid = "#{server.config[:pid]}.oldbin"
  if old_pid != server.pid
    begin
      sig = (worker.nr + 1) >= server.worker_processes ? :QUIT : :TTOU
      Process.kill(sig, File.read(old_pid).to_i)
    rescue Errno::ENOENT, Errno::ESRCH
    end
  end

  if defined?(Resque)
    Resque.redis.quit
  end

  sleep 1
end


after_fork do |server, worker|
  if defined?(ActiveRecord::Base)
    ActiveRecord::Base.establish_connection
  end
  
  if defined?(Resque)
    Resque.redis           = 'localhost:6379'
  end
end