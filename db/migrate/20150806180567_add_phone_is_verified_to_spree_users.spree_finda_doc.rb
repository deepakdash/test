# This migration comes from spree_finda_doc (originally 20150202192246)
class AddPhoneIsVerifiedToSpreeUsers < ActiveRecord::Migration
  def change
    add_column :spree_users, :phone_is_verified, :boolean, default: false
  end
end
