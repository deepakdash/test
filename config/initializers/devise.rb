Devise.setup do |config|
  # ==> Mailer Configuration
  # Configure the e-mail address which will be shown in DeviseMailer.
  config.mailer_sender = 'info@ayurvedayu.com'
  config.router_name = :spree
end

Devise.secret_key = "0041e810478423e1160f0d4c97bb7f1f9b4efc92dbeb7212e2f9e7bbdb2b67b903924d782f91e67649e0fb2f0820ecddde1d"