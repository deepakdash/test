# This migration comes from spree_finda_doc (originally 20150205202937)
class AddPhoneToSpreeVerifications < ActiveRecord::Migration
  def change
    add_column :spree_verifications, :phone, :string
  end
end
