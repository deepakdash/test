# This migration comes from spree_finda_doc (originally 20150112182730)
class CreateSpreeSuburbs < ActiveRecord::Migration
  def change
    create_table :spree_suburbs do |t|
      t.string :name
      t.belongs_to :city, index: true

      t.timestamps
    end
  end
end
