# This migration comes from spree_finda_doc (originally 20150303102045)
class AddExperienceYearsToSpreeDoctors < ActiveRecord::Migration
  def change
    add_column :spree_doctors, :experience_years, :integer
  end
end
